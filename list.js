const List = function (element , listItems){
    this.pageSize = 5;
    this.currentPage = 1;
    this.render = function (){

                    this.loadTable()
                    let buttons = element.children[1].children[1]
                    this.setupPagination(buttons) 
                    let form = element.children[1].children[2]
                    form.onsubmit = this.form_submit
                    let navigation = element.children[1].children[0]
                    navigation.children[0].onclick = this.navigateFirst
                    navigation.children[1].onclick = this.prev;
                    navigation.children[3].onclick = this.next;
                    navigation.children[4].onclick = this.navigateLast;
                    
                }

    this.loadTable = function(){
                        let tbody = element.children[0].children[1]   
                        let page_number = document.querySelector('#pagination #page-number')  
                        tbody.innerHTML = ""
                        let page = this.currentPage
                        let rows_per_page = this.pageSize
                        page --;
                        let start_loop = page*rows_per_page
                        let end_loop = start_loop + rows_per_page
                        let paginatedItems = listItems.slice(start_loop , end_loop)
                        for(let i=0 ; i<paginatedItems.length ; i++){
                            let student = paginatedItems[i]
                            let row = this.createRow(student)
                            tbody.append(row)
                        }
                        page_number.innerText = this.currentPage

                 }


    this.createRow = function(student){
                        let main_row = document.createElement('tr')
                        let roll_no = document.createElement('td')
                        let name = document.createElement('td')
                        let email = document.createElement('td')
                        roll_no.innerText = student.roll_no
                        name.innerText = student.name
                        email.innerText = student.email
                        main_row.append(roll_no)
                        main_row.append(name)
                        main_row.append(email)
                        return main_row;
    }

    this.setupPagination = function(paginated_element){
        paginated_element.innerHTML = ''
        let no_of_pages = Math.ceil(listItems.length/this.pageSize)
        for(let i=1; i<=no_of_pages; i++){
            let button = this.pageCreator(i)
            paginated_element.append(button)
        }
    }

    this.pageCreator = function(i){
        let page_button = document.createElement('button')
        page_button.innerText = i
        if(i == this.currentPage){
            page_button.classList.add('active')
        }
        return page_button;
    }

    this.next = () => {
        let last_page = Math.ceil(listItems.length/this.pageSize)
        let buttons = element.children[1].children[1]
        if(this.currentPage < last_page){
            this.currentPage++;
            this.loadTable()
            this.setupPagination(buttons)
        }
    }
    this.prev = () => {
        let buttons = element.children[1].children[1]
        if(this.currentPage > 1){
            this.currentPage--;
            this.loadTable()
            this.setupPagination(buttons)
        }
    }

    this.navigateFirst = () => {
        let buttons = element.children[1].children[1]
        this.currentPage = 1
        this.loadTable()
        this.setupPagination(buttons)
    }

    this.navigateLast = () =>{
        let buttons = element.children[1].children[1]
        let last_page = Math.ceil(listItems.length/this.pageSize)
        this.currentPage = last_page
        this.loadTable()
        this.setupPagination(buttons)
    }


    this.form_submit= (e)=>{
        let buttons = element.children[1].children[1]
        e.preventDefault()
        let row_per_page = e.target.elements.elements_per_page.value
        this.pageSize = row_per_page
        this.loadTable()
        this.setupPagination(buttons)
    }
}
